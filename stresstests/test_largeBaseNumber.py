from unittest import TestCase
from allyourbase import BaseConvert
from decimal import *
from mpmath import mp, pi

__author__ = 'david'


class TestLargeBaseNumber(TestCase):

    # 5-6 seconds
    def test_from_base_500huger(self):
        mp.dps = 50000
        numrep = BaseConvert(source_base=10, destination_base=500, max_precision=(mp.dps + 20))
        base500rep = numrep.encode(Decimal(str(+pi)), delimiter=" : ", rounding=False)
        torep = BaseConvert(source_base=500, destination_base=10, max_precision=mp.dps)
        base10rep = torep.decode(base500rep, delimiter=" : ")
        self.assertEqual(Decimal(str(+pi)), base10rep)

    # 30 minutes or more
    def test_from_base_360hugest(self):
        mp.dps = 1000000
        numrep = BaseConvert(source_base=10, destination_base=360, max_precision=(mp.dps + 20))
        base360rep = numrep.encode(Decimal(str(+pi)), delimiter=" : ", rounding=False)
        torep = BaseConvert(source_base=360, destination_base=10, max_precision=mp.dps)
        base10rep = torep.decode(base360rep, delimiter=" : ")
        self.assertEqual(Decimal(str(+pi)), base10rep)
